package code;

import java.util.*;

public class HotelBookingsPossible {
    public static void main(String[] args) {
        Solution myObject = new Solution();
        ArrayList<Integer> a = new ArrayList<>(Arrays.asList(1, 2, 3));
        ArrayList<Integer> d = new ArrayList<>(Arrays.asList(2, 3, 4));
        myObject.hotel(a, d, 2);
    }

}

class Solution {
    public boolean hotel(ArrayList<Integer> arrive, ArrayList<Integer> depart, int K) {

        List<Event> events = new ArrayList<>();
        for (int i = 0; i<arrive.size();i++){
            events.add(new Event(arrive.get(i), 0));
            events.add(new Event(depart.get(i), 1));
        }

        Collections.sort(events, new Comparator<Event>() {
            public int compare(Event e1, Event e2) {
                if (e1.time == e2.time){
                    return Integer.compare(e2.type, e1.type);
                }
                return Integer.compare(e1.time, e2.time);
            }
        });

        int count = 0;
        for (Event event: events){
            if (event.type == 1){
                count --;
            } else {
                count ++;
            }

            if (count > K){
                return false;
            }
        }

        return true;
    }
}

class Event {
    public int time;
    public int type;

    public Event(int time, int type) {
        this.time = time;
        this.type = type;
    }
}